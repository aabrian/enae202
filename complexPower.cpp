#include <iostream> 
using namespace std;

struct Complex {
    float real;  // Used to hold real part of complex number
    float imag;  // Used to hold imag part of complex number
};

Complex cadd(Complex, Complex);
Complex cmul(Complex, Complex);
void cshow(Complex);
Complex cget(void);
Complex cpow(Complex, int);
Complex conj(Complex);

int main(void) {

    Complex z;
    int n = 0;


    cout << "Enter a complex number (real then imag): ";
    z = cget();
    cout << "Enter a power (integer): ";
    cin >> n;
    cshow(z);
    cout << " raised to the power " << n << " is ";
    cshow(cpow(z,n));
    cout << endl;
    cshow(conj(z));
    cout << " raised to the power " << n << " is ";
    cshow(cpow(conj(z),n));

 

    // Done!
    return 0;
}


//  Read data from keyboard into a Complex structure
Complex cget(void) {
    Complex z;
    cin >> z.real >> z.imag;
    return z;  // we are allowed to return a single variable... now this single variable happens to contain
               // multiple things, but it's still one variable!
}

//  Formatted display of data in a Complex structure
void cshow(Complex z) {
    cout << z.real;
    if (z.imag > 0) {
        cout << "+" << z.imag << "j";
    }
    else if (z.imag < 0) {
        cout << z.imag << "j";   // remember that the minus sign is printed automatically here...
    }
}

//  Addition
Complex cadd(Complex x, Complex y) {
    //  Function for doing z = x + y
    Complex z;
    // Implement rules of complex number addition
    z.real = x.real + y.real;
    z.imag = x.imag + y.imag;
    return z;
}

//  Multiplication
Complex cmul(Complex x, Complex y) {
    //  Function for doing z = x * y
    //  Remember z = (xr+j*xi)*(yr+j*yi) = xr*yr + xr*j*yi + j*xi*yr + j*xi*j*yi
    //             = (xr*yr-xi*yi) + j*(xr*yi+xi*yr)
    Complex z;
    // Implement rules of complex number multiplication
    z.real = x.real * y.real - x.imag * y.imag;
    z.imag = x.real * y.imag + y.real * x.imag;
    return z;
}

// Implement power rule for complex numbers
Complex cpow(Complex x, int n) {
    Complex z;
    z.real = x.real;
    z.imag = x.imag;
    if (n == 0) {
        z.real = 1;
        z.imag = 0;
        return z;
    }
    else {
        for (int i = 0; i < n-1; i++) {
            z = cmul(z,x);
        }
        return z;   
    }
}
Complex conj(Complex x) {
    Complex z;
    z.real = x.real;
    z.imag = -x.imag;
    return z;
}
