// ant.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
float mindis(float*,float*,float ,float, int,int&);
int main()
{
	int N;
	std::ifstream fin("data.txt");
	if (!fin.is_open()) {
		std::cout << "Error opening file -- existing" << std::endl;
		return 1;
	}
	fin >> N;
	//std::cout << N<<std::endl<<std::endl;
	float* t = new float[N];
	float* x = new float[N];
	float* y = new float[N];
	for (int i = 0; i < N; i++) {
		fin >> t[i];
		fin >> x[i];
		fin >> y[i];
		/*std::cout << t[i] << std::setw(13);
		std::cout << x[i] << std::setw(13);
		std::cout << y[i]<< std::endl;*/
	}
	float X, Y;
	std::cout << "Enter x & y position: ";
	std::cin >> X >> Y;
	int index = 0;
	float minDist = mindis(x, y, X, Y, N, index);
	std::cout << "The smallest distance is " << minDist << " cm, which occurs at "<<t[index] << " seconds.";
}

float mindis(float* x, float* y, float X, float Y, int n, int& index) {
	float minDist = 0, initDist = 0;
	initDist = sqrt(pow(X - x[0], 2) + pow(Y - y[0], 2));
	for (int i = 1; i < n; i++) {
		minDist = sqrt(pow(X - x[i], 2) + pow(Y - y[i], 2));
		if (minDist < initDist) {
			initDist = minDist;
			index = i;
		}
		if (minDist > initDist) {
			minDist = initDist;
		}

	}
	return minDist;
}
